USE ModernWays;
CREATE TABLE Metingen(
	Tijdstip DATETIME NOT NULL,
    Grootte SMALLINT NOT NULL,
    Marge FLOAT(3,2)
);