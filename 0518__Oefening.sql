USE ModernWays;
SET SQL_SAFE_UPDATES = 0;
ALTER TABLE Huisdieren ADD COLUMN Geluid VARCHAR(20) CHAR SET utf8mb4;
SELECT * FROM huisdieren;
UPDATE huisdieren SET Geluid = "WAF!" WHERE Soort = "hond";
UPDATE huisdieren SET Geluid = "miauwww..." WHERE Soort = "kat";
SELECT * FROM huisdieren;